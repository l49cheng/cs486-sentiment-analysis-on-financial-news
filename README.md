# CS486-sentiment-analysis-on-financial-news

CS 486 project for Spring 2020.

Group: 18

Member: Lu (Lucy) Cheng, Weijia (Alissa) Kong, Yifan (Carol) Zhu

Topic: Using NLP to Perform Sentiment Analysis on Financial News Headlines

Dataset origin: https://www.kaggle.com/ankurzing/sentiment-analysis-for-financial-news

## Libraries Required:
- sklearn
- numpy
- gensim
- matplotlib

## Data Preprocessing
- Run `python3 datapreprocessing.py` with all-full.csv in the same folder
- output should be model1 and container.csv

## Perform Sentiment Analysis
- Load CS486-D3.ipynb into Jupyter notebook or Google Colab
- Click `run all` in runtime
- Upload 'all-full.csv', 'container.csv' and model1
- Output should be graphs within the google colab file.
- Note: Graph visulizations should already be available upon opening the ipynb file.
