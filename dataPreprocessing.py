import csv
from nltk.corpus import stopwords
from gensim.models import word2vec
import gensim.models
import nltk
import re
import os
import numpy as np

# input_file_path = 'all-full.csv'
#first five records
input_file_path = 'all-test.csv'
#store cut words with no stop words, numbers, etc.
clean_staging = 'container.csv'
#print statements flag
TESTING = True

header = {
    'Label' : 0,
    'News' : 1
}

#Input: csv file (input_file_path)
#output: list of lists with no header
#Ex: [0, 'sentence']
def read_data():
    with open(input_file_path, 'r') as read_obj:
        csv_reader = csv.reader(read_obj)
        data = list(csv_reader)
    result = []
    for idx, row in enumerate(data[1:]):
        record = []
        record.append(int(row[0]))
        words = wordList(row[1], True)
        sent = ' '.join(words)
        record.append(sent)
        result.append(record)
    return result

# remove all words not letters, all lower case, stop words removed
def wordList(data, stopWordRemoval = True):
    text = re.sub("[^a-zA-Z]"," ", data)
    words = text.lower().split()
    if stopWordRemoval:
        stops = set(stopwords.words("english"))
        words = [w for w in words if not w in stops]
    return words

'''
Original Example:

The international electronic industry company Elcoteq has laid off tens of employees from its Tallinn facility ; 
contrary to earlier layoffs the company contracted the ranks of its office workers , 
the daily Postimees reported 
'''
'''
Cut Word, stop words, non-letter removed Example:

international electronic industry company elcoteq laid tens employees tallinn facility 
contrary earlier layoffs company contracted ranks office workers 
daily postimees reported
'''

#container stores: lines of clean sentences, see example above
def tmp_file():
    try:
        os.remove(clean_staging)
    except OSError:
        pass
    lst = read_data()
    tst = [sent[1] for sent in lst]
    with open(clean_staging, "w+") as tmp:
        for line in tst:
            tmp.write(line)
            tmp.write('\n')
        tmp.close()

#Using existing records to train the model, and save as 'model1' for future use
def word2vec():
    sent = gensim.models.word2vec.Text8Corpus(clean_staging)
    model = gensim.models.word2vec.Word2Vec(sent, size = 200, min_count=1)
    model.save('model1')

#aim to check the training result with the given word
def checkSimilarity() :
    model = gensim.models.word2vec.Word2Vec.load('model1')
    if TESTING:
        idx = model.most_similar('revenue', topn=10)
        for i in idx:
            print(i)

def wordToSentVec():
    # combine tags from original input and clean, cut sentences
    with open(input_file_path, 'r') as read_obj_1:
        csv_reader = csv.reader(read_obj_1)
        tag = list(csv_reader)
    with open(clean_staging, 'r') as read_obj:
        csv_reader = csv.reader(read_obj)
        words = list(csv_reader)
    dataSet = []
    for idx, line in enumerate(tag[1:]):
        rec = [line[0], words[idx][0].split()]
        dataSet.append(rec)
    #load model and transform sentence into matrix
    model = gensim.models.word2vec.Word2Vec.load('model1')
    #[tag, [matrices for each word in each sentence]]
    rawNum = []
    # [tag, [average of matrices for each word in each sentence]]
    rawSum = []
    for rec in dataSet:
        senvec = []
        for word in rec[1]:
            try:
                senvec.append(model[word])
            except:
                pass
        matrix = np.array(senvec, dtype='float')
        rawNum.append([rec[0], matrix])
        avgSent = 0 if len(matrix) == 0 else (sum(matrix) / len(matrix))
        rawSum.append([rec[0], avgSent])
    print(rawNum)
    print(rawSum)

if __name__ == "__main__":
    checkSimilarity()